from threading import Thread

class jobWorker(Thread):
    stop_condition = False

    def __init__(self, _queue):
        Thread.__init__(self)
        self.queue = _queue

    def run(self):
        while True and not self.stop_condition:
            msg = self.queue.get()
            if msg == "stop":
                self.stop_condition = True
            try:
                #print(msg)
                pass
            finally:
                self.queue.task_done()
    