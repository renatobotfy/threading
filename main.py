#! /usr/bin/env python3

# for threading
from queue import Queue
from threading import Thread
import time
from jobWorker import jobWorker

class JobDispatcher():

    threadInit = False
    queue = Queue()
    workers_list = list()

    def startWorkers(self, workers_num, size):
        ts = time.time()

        if not self.threadInit:
            self.threadInit = True
            for x in range(workers_num):
                worker = jobWorker(self.queue)
                self.workers_list.append(worker)
                #worker.daemon = False
                worker.start()

        # populating queue
        for i in range(1, size):
            self.queue.put("Hello {}".format(i))

        self.queue.join()
        for entry in self.workers_list:
            self.queue.put("stop")

        sendTime = time.time() - ts
        print("data sent in {}s".format(sendTime))
    
if __name__ == "__main__":
    print("Starting")
    dispatcher = JobDispatcher()
    dispatcher.startWorkers(workers_num=4, size=1000000)